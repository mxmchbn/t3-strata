<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Contactform.Contactform',
            'Contactform',
            [
                'Contact' => 'show, send'
            ],
            // non-cacheable actions
            [
                'Contact' => 'show, send'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    contactform {
                        iconIdentifier = contactform-plugin-contactform
                        title = LLL:EXT:contactform/Resources/Private/Language/locallang_db.xlf:tx_contactform_contactform.name
                        description = LLL:EXT:contactform/Resources/Private/Language/locallang_db.xlf:tx_contactform_contactform.description
                        tt_content_defValues {
                            CType = list
                            list_type = contactform_contactform
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'contactform-plugin-contactform',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:contactform/Resources/Public/Icons/user_plugin_contactform.svg']
			);
		
    }
);
