
plugin.tx_contactform_contactform {
    view {
        # cat=plugin.tx_contactform_contactform/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:contactform/Resources/Private/Templates/
        # cat=plugin.tx_contactform_contactform/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:contactform/Resources/Private/Partials/
        # cat=plugin.tx_contactform_contactform/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:contactform/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_contactform_contactform//a; type=string; label=Default storage PID
        storagePid =
    }
    settings {
        # cat=Contactform/settings; type=string; label=Contact form email subject
        subject = New contact form submission
        # cat=Contactform/settings; type=string; label=Email of contact form sender
        fromEmail =
        # cat=Contactform/settings; type=string; label=Name of contact form sender
        fromName = Contactform
        # cat=Contactform/settings; type=string; label=Email address of contact form receipient
        toEmail =
        # cat=Contactform/settings; type=string; label=Name of contact form receipient
        toName = Contactform
        # cat=Contactform/settings; type=int; label=UID of contact form target/confirmation page
        targetPageUid =
        # cat=Contactform/settings; type=boolean; label=Use user mail address for from mail address
        useUserMailAddress =
        # cat=Contactform/settings; type=boolean; label=Persist mail to database
        persist =
    }
}
