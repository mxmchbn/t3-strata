plugin.tx_contactform_contactform {
    view {
        templateRootPaths.0 = EXT:contactform/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_contactform_contactform.view.templateRootPath}
        partialRootPaths.0 = EXT:contactform/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_contactform_contactform.view.partialRootPath}
        layoutRootPaths.0 = EXT:contactform/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_contactform_contactform.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_contactform_contactform.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        subject = {$plugin.tx_contactform_contactform.settings.subject}
        fromEmail = {$plugin.tx_contactform_contactform.settings.fromEmail}
        fromName = {$plugin.tx_contactform_contactform.settings.fromName}
        toEmail = {$plugin.tx_contactform_contactform.settings.toEmail}
        toName = {$plugin.tx_contactform_contactform.settings.toName}
        targetPageUid = {$plugin.tx_contactform_contactform.settings.targetPageUid}
        useUserMailAddress = {$plugin.tx_contactform_contactform.settings.useUserMailAddress}
        persist = {$plugin.tx_contactform_contactform.settings.persist}
    }
}

# these classes are only used in auto-generated templates
plugin.tx_contactform._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        border: 2px #FF0000 solid !important;
    }

    input.f3-form-error {
        border: 2px #FF0000 solid !important;
    }

    div.f3-error {
        color:#FF9F9F;
    }

    .tx-contactform table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-contactform table th {
        font-weight:bold;
    }

    .tx-contactform table td {
        vertical-align:top;
    }

    .typo3-messages .alert-error {
        color:red;
    }

    .typo3-messages .alert-success {
        color:green;
    }
)
