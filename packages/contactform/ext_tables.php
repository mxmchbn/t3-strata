<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Contactform.Contactform',
            'Contactform',
            'Contact Form'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('contactform', 'Configuration/TypoScript', 'Contact Form');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_contactform_domain_model_contact', 'EXT:contactform/Resources/Private/Language/locallang_csh_tx_contactform_domain_model_contact.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_contactform_domain_model_contact');

    }
);
