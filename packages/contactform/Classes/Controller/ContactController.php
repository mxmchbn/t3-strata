<?php
namespace Contactform\Contactform\Controller;

use Contactform\Contactform\Domain\Model\Contact;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***
 *
 * This file is part of the "Contact Form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * ContactController
 */
class ContactController extends ActionController
{
    /**
     * mailService
     *
     * @var \Contactform\Contactform\Service\MailService
     * @inject
     */
    protected $mailService = null;

    /**
     * contactRepository
     *
     * @var \Contactform\Contactform\Domain\Repository\ContactRepository
     * @inject
     */
    protected $contactRepository = null;

    /**
     * action show
     *
     * @return void
     */
    public function showAction()
    {
        /** @var \Contactform\Contactform\Domain\Model\Contact $contact */
        $contact = GeneralUtility::makeInstance(
            'Contactform\Contactform\Domain\Model\Contact'
        );

        $this->view->assign('show', $contact);
    }

    /**
     * action send
     *
     * @param \Contactform\Contactform\Domain\Model\Contact $contact
     */
    public function sendAction(Contact $contact)
    {
        if (!$this->mailService->send($contact)) {
            $this->addFlashMessage(
                'Something went wrong! Please, try again',
                '',
                FlashMessage::WARNING,
                true
            );
        } else {
            $this->addFlashMessage(
                'Success! We will reply you as soon as possible',
                '',
                FlashMessage::OK,
                true
            );
        }

        if ($this->settings['persist']) {
            $this->contactRepository->add($contact);
        }

        if ($this->settings['targetPageId'] != 0) {
            $uriBuilder = $this->getControllerContext()->getUriBuilder();
            $uriBuilder->reset();
            $uriBuilder->setTargetPageUid($this->settings['targetPageId']);

            $this->redirectToUri($uriBuilder->build());
        }

        $this->redirect('show');
    }
}
