<?php

namespace Contactform\Contactform\Service;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

use Contactform\Contactform\Domain\Model\Contact;

class MailService implements \TYPO3\CMS\Core\SingletonInterface
{
    use ViewTrait;

    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager = null;

    /**
     * @param Contact $contact
     * @return bool
     */
    public function send(Contact $contact)
    {
        $settings = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'contactform', 'contactform'
        );

        $fromEmail = $settings['useUserMailAdress'] ? $contact->getEMail() : $settings['fromEmail'];
        $fromName = $settings['fromName'];
        $charset = $settings['charset'];
        $subject = $settings['subject'];
        $toEmail = $settings['toEmail'];
        $toName = $settings['toName'];

        $htmlView = $this->getView('Mail/Contact', 'html');
        $htmlView->assign('contact', $contact);
        $htmlView->assign('charset', $charset);
        $htmlView->assign('subject', $subject);
        $htmlBody = $htmlView->render();

        $plainView = $this->getView('Mail/Contact', 'txt');
        $plainView->assign('contact', $contact);
        $plainView->assign('charset', $charset);
        $plainView->assign('subject', $subject);
        $plainBody = $plainView->render();

        /** @var \TYPO3\CMS\Core\Mail\MailMessage $message */
        $message = GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');
        $message->setTo(array($toEmail => $toName));
        $message->setFrom(array($fromEmail => $fromName));
        $message->setSubject($subject);
        $message->setCharset($charset);
        $message->setBody($htmlBody, 'text/html');
        $message->addPart($plainBody, 'text/plain');

        $message->send();

        return $message->isSent();
    }
}