<?php

namespace Contactform\Contactform\Service;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

trait ViewTrait
{
    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager = null;

    /**
     * @param string $templateName
     * @param string $format
     * @return \TYPO3\CMS\Fluid\View\StandaloneView
     */
    protected function getView($templateName, $format = 'html')
    {
        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $view */
        $view = GeneralUtility::makeInstance('TYPO3\CMS\Fluid\View\StandaloneView');
        $view->setFormat($format);
        $view->getRequest()->setControllerExtensionName('contactform');

        $extbaseFrameworkConfiguration = $this->configurationManager
            ->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $templateRootPath = GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPaths']['1']);
        $layoutRootPath = GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['layoutRootPaths']['1']);
        $templatePathAndFilename = $templateRootPath . $templateName . '.' . $format;

        $view->setTemplatePathAndFilename($templatePathAndFilename);
        $view->setLayoutRootPaths(array($layoutRootPath));

        return $view;
    }
}